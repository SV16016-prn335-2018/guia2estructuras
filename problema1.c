#include <stdio.h>
#include <stdlib.h>

int main(void) {

    int n, tamanio=0, contador=0;
    int validacion = 0;
    printf("\nintroduzca la dimension de las matrices que usara\n");
    scanf("%d", &n);
    int matriz1[n][n], matriz2[n][n], resultado = 0, resul[n][n];
    int i, j, c;
    for (i = 0; i < n; i++)
        for (j = 0; j < n; j++)
            resul[i][j] = 0;
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            printf("\nintroduzca un valor para la matriz #1: [%d][%d]: ", i, j);
            scanf("%d", &matriz1[i][j]);
        }
    }
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            printf("\nintroduzca un valor mat2[%d][%d]: ", i, j);
            scanf("%d", &matriz2[i][j]);
        }
    }
    for (i = 0; i < n; i++) {
        for (c = 0; c < n; c++) {
            for (j = 0; j < n; j++) {
                resultado = resultado + (matriz1[i][j] * matriz2[j][c]);
            }
            resul[i][c] = resultado;

            for (i = 1; i <= resultado; i++) {
                if (resultado % i == 0.0) {
                    validacion++;
                }
            }

            if (validacion == 2) {
                tamanio=tamanio+1;
            }
            resultado = 0;
            validacion = 0;
        }



    }
    /*Matriz 1*/ printf("\nMatriz 1\n");
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            printf("%d", matriz1[i][j]);
        }
        printf("\n");
    }
    /*Matriz 2*/
    printf("\nMatriz 2\n");
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            printf("%d", matriz2[i][j]);
        }
        printf("\n");
    }
    /*Matriz multiplicación*/
    printf("\nMatriz resultado\n");
    for (i = 0; i < n; i++) {
        for (c = 0; c < n; c++) {
            printf("%d", resul[i][c]);
        }
        printf("\n");
    }

    int primos[tamanio];
    printf("\nprimos\n");
    for (i = 0; i < n; i++) {
        for (c = 0; c < n; c++) {
            for (int k = 1; k <= resul[i][c]; k++) {
                if (resul[i][c] % k == 0.0) {
                    validacion++;
                }
            }

            if (validacion == 2) {
                printf("Numero primo en la posicion [%d][%d] = %d\n", i, c, resul[i][c]);
                primos[contador] = resul[i][c];
                contador=contador+1;
            }
            validacion = 0;


        }
        printf("\n");
    }
    if(tamanio==0){
     
                printf("\nNo se encontraron.\n");
           
    }else{
     printf("\nvector de primos:\n");
    }


    for (int c = 0; c < tamanio; c++) {
        for (int d = 0; d < tamanio - c - 1; d++) {
            if (primos[d] > primos[d + 1]) {
                int t = primos[d];
                primos[d] = primos[d + 1];
                primos[d + 1] = t;
            }
        }
    }

    for(int p=0;p<tamanio;p++){
        printf("%d. ",primos[p]);
    
    }


}
 
